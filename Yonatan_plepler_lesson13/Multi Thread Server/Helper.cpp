#include "Helper.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

using std::string;

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);

	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}


void Helper::send_update_message_to_client(SOCKET sc, const string& file_content, const string& second_username, const string &all_users)
{
	//TRACE("all users: %s\n", all_users.c_str())
	const string code = std::to_string(MT_SERVER_UPDATE);
	const string current_file_size = getPaddedNumber(file_content.size(), 5);
	const string username_size = getPaddedNumber(second_username.size(), 2);
	const string all_users_size = getPaddedNumber(all_users.size(), 5);
	const string res = code + current_file_size + file_content + username_size + second_username + all_users_size + all_users;
	//TRACE("message: %s\n", res.c_str());
	sendData(sc, res);
}

// recieve data from socket according byteSize
// returns the data as int
int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	return atoi(s);
}

// recieve data from socket according byteSize
// returns the data as string
string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	return res;
}

// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();

}



// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}


//Check if socket is open
bool Helper::is_connected(SOCKET sock)
{
	char buf;
	int err = recv(sock, &buf, 1, MSG_PEEK);
	return err == -1 ? false : true;
}

/*
This function reads the content pf the file user1&user2 (In alphabetic order)
In: the chat file name
Out: content of the file
*/
std::string Helper::getChat(std::string user1, std::string user2)
{
	char x = ' ';
	std::string sum = "";
	std::string fileName = ".txt";
	fileName = (user2.compare(user1) > 0 ? user1 + '&' + user2 : user2 + '&' + user1) + fileName;
	std::fstream chatFile;
	chatFile.open(fileName);
	if (!chatFile)
	{

	}
	else
	{
		//We use get() since >> doesn't read whitespace
		while (chatFile.get(x)) 
		{
			sum = sum + x;
		}
		chatFile.close();
	}
	return sum;
}


/*

*/
void Helper::updateChat(std::string user1, std::string user2, std::string newMsg)
{
	std::string x = "";
	std::string sum = "";
	std::string fileName = ".txt";
	fileName = (user2.compare(user1) > 0 ? user1 + '&' + user2 : user2 + '&' + user1) + fileName;
	std::ofstream chatFile;
	chatFile.open(fileName, std::fstream::out | std::ios_base::app);
	if (chatFile)
	{
		chatFile << "&MAGSH_MESSAGE&&Author&" << user1 << "&DATA&" << newMsg;
		chatFile.close();
	}
	
}