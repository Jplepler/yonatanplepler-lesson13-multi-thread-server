#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <string>
#include <fstream>
#include <exception>
#include <set>
#include <map>

using namespace std;

int main()
{

	string line;
	int srcPort = 0;
	//Set of maps
	set<map<string, string>> loggedUsers;

	ifstream socketDataFile;

	socketDataFile.open("config.txt");
	if (!socketDataFile) {
		cerr << "Unable to open file config.txt and get socket data" << endl;;
		exit(1);   // call system to stop
	}

	do
	{
		getline(socketDataFile, line);
	} while (line.find("port=") == std::string::npos);

	socketDataFile.close();

	try
	{
		srcPort = stoi(line.substr(line.find("=") + 1, line.size() - line.find("=") - 1));
	}
	catch (invalid_argument const& e)
	{
		cerr << "Couldn't find port data in file" << endl;
		exit(1);   // call system to stop
	}
	
	


	while (true)
	{
		try
		{
			WSAInitializer wsaInit;
			Server myServer;
			myServer.serve(srcPort);
		}
		catch (exception & e)
		{
			cout << "Error occured: " << e.what() << endl;
		}
	}

	system("PAUSE");
	return 0;
}