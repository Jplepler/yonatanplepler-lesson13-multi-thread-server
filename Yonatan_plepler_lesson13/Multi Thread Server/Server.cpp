#define _CRT_SECURE_NO_WARNINGS

#include "Server.h"
#include "Helper.h"



#define MAX_SIZE 100
using namespace std;
mutex usersListMutex;
mutex socketMutex;
mutex printMutex;
mutex fileMutex;

std::set <std::pair<std::string, bool >> Server::_usersList;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
		std::cout << endl;
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept
	
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	thread t(&Server::clientHandler, Server(), client_socket);
	t.detach();
}


/*
This function handles a single clients requests until he logs out
In: the socket
*/
void Server::clientHandler(SOCKET clientSocket)
{
	/*
	The try and catch in this function are used 
	to make sure not mutexes are not permanently locked.
	in case function ends (An exception is thrown) after lock the catch will unlock the mutex before the return.
	*/


	Helper h;
	char buffer[MAX_SIZE] = { 0 };
	string clientMsg = "";
	string serverMsg = "";
	string secondUser = "";
	string newMsg = "";
	string thisclient = "";

	recv(clientSocket, buffer, MAX_SIZE - 1, 0);
	if (buffer[0] == '\0')//Check if we  didn't recieve anything
	{
		return;
	}

		
	clientMsg = buffer;
	std::cout << "Client's mesage: " << clientMsg << endl;

	
	usersListMutex.lock();
	//Any problem with the string result in log out
	try {
		//Add username to set
		_usersList.insert(pair<string, bool>(clientMsg.substr(5, int(clientMsg[3])), true));
	}
	catch (...)
	{
		usersListMutex.unlock();
		std::cout << "connection failed" << endl;
		closesocket(clientSocket);
		return;
	}
	usersListMutex.unlock();

	printMutex.lock();
	
	std::cout << "Logging in: " << clientMsg.substr(5, int(clientMsg[3])) << endl;
	thisclient = clientMsg.substr(5, int(clientMsg[3]));
	cout << "Truble connecting to user" << endl;
	printMutex.unlock();

	serverMsg = "101";//Msg code
	//Deafault at login
	serverMsg += "00000";
	serverMsg += "00";
	usersListMutex.lock();
	try {
		serverMsg += this->getListLen();//Len of all names
		serverMsg += this->toString();//string of all logged users
	}
	catch (...)
	{
		usersListMutex.unlock();
		std::cout << "Problem with connection, disconnecting..." << endl;
		closesocket(clientSocket);
		_usersList.erase(std::pair<std::string, bool>(thisclient, true));
		_usersList.insert(std::pair<std::string, bool>(thisclient, false));
		return;
	}
	usersListMutex.unlock();
		
	memset(buffer, 0, MAX_SIZE);
	try {
		h.sendData(clientSocket, serverMsg);
	}
	catch(const std::exception& e)
	{
		usersListMutex.lock();
		_usersList.erase(std::pair<std::string, bool>(thisclient, true));
		_usersList.insert(std::pair<std::string, bool>(thisclient, false));
		usersListMutex.unlock();

		printMutex.lock();
		cerr << e.what() << endl;
		printMutex.unlock();
		return;
	}


	while (true)
	{
		recv(clientSocket, buffer, MAX_SIZE - 1, 0);
		clientMsg = buffer;
		if (stoi(clientMsg.substr(3, 2)) > 0)//if len_second_user is not 0
		{
			secondUser = clientMsg.substr(5, stoi(clientMsg.substr(3, 2)));
			newMsg = clientMsg.substr(10 + stoi(clientMsg.substr(3, 2)), stoi(clientMsg.substr(5 + stoi(clientMsg.substr(3, 2)), 5)));
				
			if (newMsg != "")
			{
				fileMutex.lock();
				h.updateChat(thisclient, secondUser, newMsg);
				fileMutex.unlock();
			}
				
			usersListMutex.lock();
			fileMutex.lock();
			//Incase disconected, make sure to unlock relevant mutex
			try {
				h.send_update_message_to_client(clientSocket, h.getChat(thisclient, secondUser), secondUser, toString());
			}
			catch (...)
			{
				_usersList.erase(std::pair<std::string, bool>(thisclient, true));
				_usersList.insert(std::pair<std::string, bool>(thisclient, false));
				fileMutex.unlock();
				usersListMutex.unlock();
				cout << "disconnecting from " << thisclient << endl;
				closesocket(clientSocket);
				return;
			}
				
			fileMutex.unlock();
			usersListMutex.unlock();
		}
		else
		{
			usersListMutex.lock();
			//Incase disconected, make sure to unlock relevant mutex
			try 
			{
				h.send_update_message_to_client(clientSocket, "", "", toString());
			}
			catch (...)
			{
				//Update user list (user disconnected)
				_usersList.erase(std::pair<std::string, bool>(thisclient, true));
				_usersList.insert(std::pair<std::string, bool>(thisclient, false));
				usersListMutex.unlock();
				cout << "disconnecting from " << thisclient << endl;
				closesocket(clientSocket);
				return;
			}
			usersListMutex.unlock();
		}
	}
}


/*
This function calculates the length of all users with & between them
Out: length of all users and & between them
*/
string Server::getListLen()
{
	int  i = 0;
	int len = 0;
	string str = "";
	set<pair<string, bool>>::iterator it;
	
	for (it = _usersList.begin(); it != _usersList.end(); ++it)
	{
		if ((*it).second)
		{
			len += (*it).first.size();//Add each name's length to the var
		}
		
	}
	//Add len of all &
	len += _usersList.size() - 1;

	for ( i = 0; i <5 - (len > 0 ? (int)log10((double)len) + 1 : 1); i++)
	{
		str += '0';
	}
	
	str += to_string(len);
	return str;
}


/*
This function turns the user set into string eah name seperated by &
Out: string of all users that are logged
*/
std::string Server::toString()
{
	string str = "";
	set<pair<string, bool>>::iterator it;
	for (it = _usersList.begin(); it != _usersList.end(); ++it)
	{
		if ((*it).second)
		{
			str += (*it).first + "&";
		}
	}
	str = str.substr(0, str.length() - 1);
	return str;
}

