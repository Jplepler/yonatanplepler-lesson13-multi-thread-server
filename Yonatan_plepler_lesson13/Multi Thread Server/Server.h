#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <set>
#include <string>
#include <exception>
#include <iostream>
#include <thread>
#include <Mutex>
#include <cmath>


class Server
{
public:
	Server();
	~Server();
	void serve(int line);

private:
	
	void accept();
	void clientHandler(SOCKET clientSocket);
	std::string getListLen();
	std::string toString();

	static std::set <std::pair<std::string, bool >> _usersList;
	SOCKET _serverSocket;
};

